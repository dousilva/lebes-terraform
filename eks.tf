terraform {
  required_version = ">= 0.12.0"
}

# provider "aws" {
#   version = ">= 2.28.1"
#   region  = var.region
# }

provider "random" {
  version = "~> 2.1"
}

provider "local" {
  version = "~> 1.2"
}

provider "null" {
  version = "~> 2.1"
}

provider "template" {
  version = "~> 2.1"
}


provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}

# resource "random_string" "suffix" {
#   length  = 8
#   special = false
# }

resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/16",
    ]
  }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name_prefix = "worker_group_mgmt_two"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.130.0.0/16",
    ]
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.130.0.0/16",
      "172.16.0.0/12",
      "192.168.0.0/24",
    ]
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.47.0"

  name = "eks-vpc-fintech-${local.environment}"
  cidr = "10.130.0.0/16"
  #azs                  = data.aws_availability_zones.available.names
  #enable_nat_gateway   = true
  #single_nat_gateway   = true
  enable_dns_hostnames = true
  azs                  = var.az_ids
  private_subnets      = var.private_subnets_blocks
  database_subnets     = var.database_subnets_blocks
  public_subnets       = var.public_subnets_blocks
  #enable_s3_endpoint                     = var.s3_endpoint
  enable_nat_gateway = var.nat_gateway
  single_nat_gateway = var.single_nat_gw

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "12.2.0"
  cluster_name    = local.cluster_name
  cluster_version = local.cluster_version
  subnets         = module.vpc.private_subnets

  tags = {
    Environment = "${local.environment}"
    GithubRepo  = "terraform-aws-eks"
    GithubOrg   = "terraform-aws-modules"
  }

  vpc_id = module.vpc.vpc_id

  # worker_groups = var.eks_worker_groups

  worker_groups = [

    {
      name                          = "worker-group-1"
      key_name                      = "${var.key_pair}"
      instance_type                 = "${var.eks_worker_group_instance_type}"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
      asg_desired_capacity          = "${var.eks_worker_groups_desired_capacity}"
      asg_max_size                  = "4"
    },
  ]

  #   worker_groups = [

  #     {
  #       name                          = "worker-group-2"
  #       instance_type                 = "t2.medium"
  #       additional_userdata           = "echo foo bar"
  #       additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
  #       asg_desired_capacity          = 1
  #     },
  #   ]

  worker_additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]
  map_roles                            = var.map_roles
  map_users                            = var.map_users
  map_accounts                         = var.map_accounts
}


resource "aws_autoscaling_schedule" "asg-start" {
  scheduled_action_name = "asg-start"
  min_size              = 1
  max_size              = 4
  desired_capacity      = 2
  start_time            = "2021-01-21T19:50:00Z"
  recurrence            = "0 08 * * 1,2,3,4,5"

  #end_time               = "2016-12-12T06:00:00Z"
  autoscaling_group_name = join(",", module.eks.workers_asg_names)

  count = var.environment == "prd" ? 0 : 1
}

resource "aws_autoscaling_schedule" "asg-shutdown" {
  scheduled_action_name = "asg-shutdown"
  min_size              = 0
  max_size              = 0
  desired_capacity      = 0
  start_time            = "2021-01-21T19:50:00Z"
  recurrence            = "0 01 * * *"

  #end_time               = "2016-12-12T06:00:00Z"
  autoscaling_group_name = join(",", module.eks.workers_asg_names)

  count = var.environment == "dev" ? 0 : 1
}

output "workers_asg_names" {
  value = "${module.eks.workers_asg_names}"
}