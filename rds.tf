# SG
resource "aws_security_group" "rds-fintech" {
  name_prefix = "rds-fintech-${local.environment}"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    cidr_blocks = [
      "10.130.0.0/16",
    ]
  }
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "fintechdb-${local.environment}"

  engine             = "postgres"
  engine_version     = var.rds_engine_version
  instance_class     = var.instance_class
  allocated_storage  = var.rds_allocated_storage
  create_db_instance = var.create_db_instance_rds
  name               = "fintech"
  username           = "root"
  password           = var.password
  port               = var.port

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [aws_security_group.rds-fintech.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval    = "30"
  monitoring_role_name   = "fintechRDSMonitoringRole-${var.environment}"
  create_monitoring_role = true

  tags = {
    Owner       = "fintech"
    Environment = "${local.environment}"
  }

  # DB subnet group
  #subnet_ids = ["subnet-12345678", "subnet-87654321"]
  subnet_ids  = module.vpc.database_subnets

  # DB parameter group
  family = "postgres9.6"

  # DB option group
  major_engine_version = "9.6"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "demodb-${var.environment}"

  # Database Deletion Protection
  deletion_protection = false

 

}

