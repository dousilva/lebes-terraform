locals {
  # vpc_name    = "${var.initials}-${var.vpc_name_id}-${var.project}-${var.environment}"
  # sg_name     = "${var.initials}-${var.sg_name}-${var.project}-${var.environment}"
  environment = var.environment
  stack       = var.stack
  region      = var.region
  tags        = var.tags
  
  cluster_name    = "eks-fintech-${var.environment}"
  cluster_version = var.cluster_version
  #subnets_eks     = ["subnet-02700af551ce51bfd", "subnet-0265f3e06911d0588", "subnet-05c4c6c20029693f9"]
  #subnets_eks     = data.aws_subnet_ids.private.ids

}

data "aws_availability_zones" "available" {
}


# data "aws_availability_zones" "azs" {
#   state = "available"
# }

# data "aws_availability_zones" "available" {
#   state = "available"
# }

# Para usar subnet e VPC existentes.
# data "aws_subnet_ids" "private" {
#   vpc_id = "vpc-0e724b9399f451c02"

#   tags = {
#     Name = "parser-VPCPortoSegGestaoRiscoParser-parser-dev-private*"

#     #Tier = "private"
#   }
# }

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}



