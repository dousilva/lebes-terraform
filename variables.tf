#Gerais
variable "account" {
  type        = string
  description = ""
}
variable "environment" {
  type        = string
  description = ""
}

variable "project" {
  type        = string
  description = ""
}

variable "initials" {
  type        = string
  description = ""
}

variable "cidr" {
  type        = string
  description = ""
  default     = "10.20.0.0/16"
}

variable "stack" {
  type        = string
  description = ""
}
variable "region" {
  type        = string
  description = "us-east-2"
}

variable "region-aws" {
  type        = string
  description = ""
}

variable "tags" {
  type        = map
  description = ""
  default     = {}
}

#vpc
variable "azs" {
  type        = list
  description = ""
  default     = ["us-east-2a"]
}

variable "private_subnets" {
  type        = list
  description = ""
  default     = ["10.0.1.0/24"]
}

variable "public_subnets" {
  type        = list
  description = ""
  default     = ["10.0.101.0/24"]
}

variable "enable_nat_gateway" {
  type        = bool
  description = ""
  default     = true
}
variable "enable_vpn_gateway" {
  type        = bool
  description = ""
  default     = false
}

#SG
variable "sg_name" {
  type        = string
  description = ""
  default     = "sg_vpc"
}

variable "sg_description" {
  type        = string
  description = ""
  default     = "Security Group"
}

variable "ingress_cidr_blocks" {
  type        = list
  description = ""
  default     = ["10.10.0.0/16"]
}

variable "ingress_rules" {
  type        = list
  description = ""
  default     = ["https-443-tcp"]
}

variable "ingress_with_cidr_blocks" {
  type        = list
  description = ""
  default     = []
}

variable "vpc_name_id" {
  type        = string
  description = ""
}
variable "cidr_blocks" {
  type        = string
  description = ""
}
variable "az_ids" {
  type        = list
  description = ""
}
variable "database_subnets_blocks" {
  type        = list
  description = ""
}
variable "create_db_subnet_group" {
  type        = bool
  description = ""
}
variable "create_db_subnet_route_table" {
  type        = bool
  description = ""
}
variable "create_db_internet_gateway_route" {
  type        = bool
  description = ""
}
variable "create_db_nat_gateway_route" {
  type        = bool
  description = ""
}
variable "private_subnets_blocks" {
  type        = list
  description = ""
}

variable "public_subnets_blocks" {
  type        = list
  description = ""
}
variable "s3_endpoint" {
  type        = bool
  description = ""
}
variable "nat_gateway" {
  type        = bool
  description = ""
}
variable "single_nat_gw" {
  type        = bool
  description = ""
}
variable "one_nat_gw_per_az" {
  type        = bool
  description = ""
}
variable "vpn_gateway" {
  type        = bool
  description = ""
}
variable "nat_ips_reuse" {
  type        = bool
  description = ""
}
variable "count_create_nat_ips" {
  type        = number
  description = ""
}
variable "nat_ip_for_vpc" {
  type        = bool
  description = ""
}
# S3
variable "s3_raw_acl" {
  type        = string
  description = ""
  default     = "private"
}

variable "s3_raw_versioning" {
  type        = map
  description = ""
  default     = {}
}

variable "s3_raw_lifecycle_rule" {
  type        = list
  description = ""
  default     = []
}

# # variable "region" {
# #   default = "us-west-2"
# # }

#EKS

variable "key_pair" {
  default = "fix-eks"

}

variable "eks_worker_group_instance_type" {
  default = ""
}

variable "eks_worker_groups_desired_capacity" {
  default = ""
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "436125626616",
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::436125626616:role/ClaranetCrossN3Fixbr"
      username = "role1"
      groups   = ["system:masters"]
    },
    {
      rolearn  = "arn:aws:iam::436125626616:role/ClaranetCrossN2Fixbr"
      username = "role1"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::436125626616:user/jenkins"
      username = "user1"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::495770326048:user/izack"
      username = "user2"
      groups   = ["system:masters"]
    },
  ]
}

variable "cluster_version" {
  default = "1.17"
}

# RDS

variable "create_cluster_aurora" {
  default = false
}

variable "create_db_instance_rds" {
  default = false
}

variable "instance_type_aurora" {
  
}

variable "rds_engine_version" {

}

variable "password" {
  default = ""
}

variable "port" {
  default = "3306"
}

variable "instance_class" {
  default = "db.t2.micro"
}

variable "rds_allocated_storage" {
  default = "10"
}

# REDIS

variable "cluster_size" {
  type        = number
  default     = 1
  description = "Number of nodes in cluster. *Ignored when `cluster_mode_enabled` == `true`*"
}

variable "instance_type" {
  type        = string
  default     = "cache.t3.micro"
  description = "Elastic cache instance type"
}

variable "family" {
  type        = string
  default     = "redis5.0"
  description = "Redis family"
}

variable "parameter" {
  type = list(object({
    name  = string
    value = string
  }))
  default     = []
  description = "A list of Redis parameters to apply. Note that parameters may differ from one Redis family to another"
}

variable "redis_engine_version" {
  type        = string
  default     = "5.0.6"
  description = "Redis engine version"
}

variable "at_rest_encryption_enabled" {
  type        = bool
  default     = false
  description = "Enable encryption at rest"
}

variable "transit_encryption_enabled" {
  type        = bool
  default     = false
  description = "Enable TLS"
}

variable "cloudwatch_metric_alarms_enabled" {
  type        = bool
  description = "Boolean flag to enable/disable CloudWatch metrics alarms"
  default     = false
}

variable "zone_id" {
  type        = string
  default     = ""
  description = "Route53 DNS Zone ID"
}

variable "availability_zones" {
  type        = list(string)
  description = "Availability zone IDs"
  default     = ["us-east-1a", "us-east-1c"]
}
