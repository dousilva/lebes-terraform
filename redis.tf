# SG
resource "aws_security_group" "redis-fintech" {
  name_prefix = "redis-fintech-${local.environment}"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 6379
    to_port   = 6379
    protocol  = "tcp"

    cidr_blocks = [
      "10.130.0.0/16",
    ]
  }
  egress {

    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

module "redis" {
  source             = "git::https://github.com/cloudposse/terraform-aws-elasticache-redis.git?ref=0.23.0"
  availability_zones = var.availability_zones
  //namespace          = var.namespace
  name = "redis-fintech-${var.environment}"
  // zone_id                    = var.zone_id
  vpc_id                     = module.vpc.vpc_id
  allowed_security_groups    = [aws_security_group.redis-fintech.id]
  subnets                    = module.vpc.database_subnets
  cluster_size               = var.cluster_size
  instance_type              = var.instance_type
  apply_immediately          = true
  automatic_failover_enabled = false
  engine_version             = var.redis_engine_version
  family                     = var.family
  at_rest_encryption_enabled = var.at_rest_encryption_enabled
  transit_encryption_enabled = var.transit_encryption_enabled

  parameter = [
    {
      name  = "notify-keyspace-events"
      value = "lK"
    }
  ]
}
