#Gerais
environment = "hml"
stack       = "devops"
region      = "south-america"
region-aws  = "sa-east-1"
account     = "919342241657"
initials    = "Fintech"
project     = "devops"

# S3
# s3_raw_acl = "private"
# s3_raw_versioning = {
#   enabled = "true"
# }
# s3_raw_lifecycle_rule = [{
#   id      = "bucket_raw"
#   enabled = true
#   prefix  = ""
#   transition = [
#     {
#       days          = 60
#       storage_class = "STANDARD_IA"
#     },
#     {
#       days          = 90
#       storage_class = "GLACIER"
#     }
#   ]
# }]


# #Sg
# sg_name             = "sg-vpc"
# sg_description      = "Security group for user-service with custom ports open within VPC"
# ingress_cidr_blocks = ["10.10.0.0/16"]
# ingress_rules       = ["https-443-tcp"]
# ingress_with_cidr_blocks = [
#   {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     description = "ssh"
#     cidr_blocks = "10.10.0.0/16"
#   }
# ]

#EKS
cluster_version                    = "1.17"
eks_worker_group_instance_type     = "t3a.medium"
eks_worker_groups_desired_capacity = 2


#VPC
vpc_name_id                      = "vpc"
cidr_blocks                      = "10.130.0.0/16"
az_ids                           = ["sa-east-1", ]
private_subnets_blocks           = ["10.130.1.0/24", "10.130.2.0/24"]
database_subnets_blocks          = ["10.130.3.0/24", "10.130.6.0/24"]
public_subnets_blocks            = ["10.130.4.0/24", "10.130.5.0/24"]
enable_nat_gateway               = true
enable_vpn_gateway               = false
create_db_subnet_group           = true
create_db_subnet_route_table     = true
create_db_internet_gateway_route = false
create_db_nat_gateway_route      = false
s3_endpoint                      = true
nat_gateway                      = true
single_nat_gw                    = true
one_nat_gw_per_az                = false
vpn_gateway                      = false
nat_ips_reuse                    = true # <= Skip creation of EIPs for the NAT Gateways
count_create_nat_ips             = 1
nat_ip_for_vpc                   = true

tags = {
  "System ID"           = "template"
  "System"              = "CPS"
  "Key-user"            = "CPS"
  "Cost Center Project" = "CPS"
  "Parent System ID"    = "CPS"
  "Parent System"       = "CPS"
  "Environment"         = "dev"
  "Domain"              = "CPS"
  "csc_bkp"             = "ESS"
  "csc_mon"             = "ESS"
}

# RDS
rds_engine_version      = "5.7.30"
instance_class          = "db.t2.large"
rds_allocated_storage   = 5
password                = "1w28GhPyU612"
port                    = "5432"
create_cluster_aurora   = false
create_db_instance_rds  = true

cluster_size         = 1
instance_type        = "cache.t3.micro"
redis_engine_version = "5.0.6"
availability_zones = ["sa-east-1a", "sa-east-1c"]

