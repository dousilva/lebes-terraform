#Gerais
environment = "dev"
stack       = "devops"
region      = "south-america"
region-aws  = "sa-east-1"
account     = "919342241657"
initials    = "Fintech"
project     = "devops"

#EKS
cluster_version                    = "1.17"
eks_worker_group_instance_type     = "t3a.medium"
eks_worker_groups_desired_capacity = 2
key_pair = "fintech-eks-dev"


#VPC
vpc_name_id                      = "vpc"
cidr_blocks                      = "10.130.0.0/16"
az_ids                           = ["sa-east-1a", "sa-east-1b" ]
private_subnets_blocks           = ["10.130.1.0/24", "10.130.2.0/24"]
database_subnets_blocks          = ["10.130.3.0/24", "10.130.4.0/24"]
public_subnets_blocks            = ["10.130.5.0/24", "10.130.6.0/24"]
enable_nat_gateway               = true
enable_vpn_gateway               = false
create_db_subnet_group           = true
create_db_subnet_route_table     = true
create_db_internet_gateway_route = false
create_db_nat_gateway_route      = false
s3_endpoint                      = true
nat_gateway                      = true
single_nat_gw                    = false
one_nat_gw_per_az                = true
vpn_gateway                      = false
nat_ips_reuse                    = true # <= Skip creation of EIPs for the NAT Gateways
count_create_nat_ips             = 1
nat_ip_for_vpc                   = true


# RDS
engine              = "postgres"
rds_engine_version  = "9.6"
instance_class      = "db.m5.large"
allocated_storage   = 30
password               = "qD33JgYpk812"
port                   = "5432"
create_cluster_aurora  = false
create_db_instance_rds = true
instance_type_aurora   = "db.m5.large"

# REDIS
cluster_size         = 2
instance_type        = "cache.t3.medium"
redis_engine_version = "5.0.6"
availability_zones = ["sa-east-1a", "sa-east-1b"]

tags = {
  "System ID"           = "template"
  "System"              = "CPS"
  "Key-user"            = "CPS"
  "Cost Center Project" = "CPS"
  "Cost Center UN1"     = "CPS"
  "Cost Center UN2"     = "CPS"
  "Cost Center UN3"     = "CPS"
  "Parent System ID"    = "CPS"
  "Parent System"       = "CPS"
  "Environment"         = "prd"
  "Domain"              = "CPS"
  "csc_bkp"             = "ESS"
  "csc_mon"             = "ESS"
}

#DEV