resource "aws_dynamodb_table" "mensagem-table" {
  name           = "mensagem"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "mensagemId"
  range_key      = "layouId"

  attribute {
    name = "layouId"
    type = "S"
  }

  #   attribute {
  #     name = "tipo"
  #     type = "S"
  #   }

  attribute {
    name = "status"
    type = "S"
  }

  attribute {
    name = "mensagemId"
    type = "N"
  }

  attribute {
    name = "servicoId"
    type = "N"
  }

  global_secondary_index {
    name     = "statusIndex"
    hash_key = "status"
    // range_key          = "TopScore"
    write_capacity  = 1
    read_capacity   = 1
    projection_type = "KEYS_ONLY"
    //non_key_attributes = ["mensagemId", "layouId", "servicoId"]
  }


  global_secondary_index {
    name     = "servicoIdIndex"
    hash_key = "servicoId"
    // range_key          = "TopScore"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "INCLUDE"
    non_key_attributes = ["mensagemId", "layouId", "status"]
  }

  tags = {
    Name        = "dynamodb-table-mensagem"
    Environment = "${var.environment}"
  }

}


resource "aws_dynamodb_table" "change_log-table" {
  name           = "change_log"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "entidadeId"
  range_key      = "entidade"
  // non_key_attributes = ["status", "servicoId", "tipo"]

  attribute {
    name = "entidade"
    type = "S"
  }


  attribute {
    name = "usuarioId"
    type = "N"
  }

  attribute {
    name = "entidadeId"
    type = "N"
  }

  global_secondary_index {
    name     = "usuarioIdIndex"
    hash_key = "usuarioId"
    // range_key          = "TopScore"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "INCLUDE"
    non_key_attributes = ["entidadeId", "entidade"]
  }



  tags = {
    Name        = "dynamodb-table-change_log"
    Environment = "${var.environment}"
  }

}
